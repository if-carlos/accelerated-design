filter: 
omit: 
include: 
exclude: 

A11y ID: color-contrast
description: Ensures the contrast between foreground and background colors meets WCAG 2 AA contrast ratio thresholds
Detected on:
    http://localhost:6006/?path=/story/molecules-accordions--js-accordions
