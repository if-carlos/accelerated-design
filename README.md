## Accelerated Design

Accelerated Design is the default Emulsify system for Interpersonal Frequency Drupal projects.

## Development (Lando)

- Run `lando start`

That should spin up a local server at `http://accelerated-design.lndo.site` that you can open in your browser to see the components in a Storybook instance.  It will also watch for updates to your code, so any changes you make will be reflected in the Storybook instance live, any time you save a file.

## Development (no Lando)

- Ensure you're using the correct node/npm version `nvm use`
- Install dependencies `npm install`
- Run the develop script `npm run develop`

That should spin up a local server (typically at `http://localhost:6006`) that you can open in your browser, to see the components in a Storybook instance. It will also watch for updates to your code, so any changes you make will be reflected in the Storybook instance live, any time you save.
