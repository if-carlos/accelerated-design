import accordions from './accordions.twig';

import accordionData from './accordions.yml';

import './accordions';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Accordions' };

export const JSAccordions = () => accordions(accordionData);
